import sqlite3
from typing import List
import time


con = sqlite3.connect("./db/py_db.sqlite3")

from dataclasses import dataclass

@dataclass
class Dog:
    id: str
    first_name: str
    last_name: str
    age: int
    sex: int
    temperament: int


def import_dogs() -> List[Dog]:
    with open("assets/poms.csv", "r") as f:
        dogs_data = f.readlines()
    
    dogs = []
    for dog_data in dogs_data:
        id, first_name, last_name, age, sex, temperament = dog_data.split(",")
        dogs.append(
            Dog(id, first_name, last_name, age, sex, temperament)
        )
    
    return dogs


def create_table():
    stmt = """
    PRAGMA journal_mode=WAL;
    PRAGMA busy_timeout=5000;
    PRAGMA foreign_keys=1;

    CREATE TABLE IF NOT EXISTS dog (
		id INT PRIMARY KEY,
		uuid TEXT UNIQUE NOT NULL,
		first_name TEXT NOT NULL,
		last_name TEXT NOT NULL,
		age INT NOT NULL,
		sex INT NOT NULL,
		temperament INT NOT NULL,
		created_date TEXT DEFAULT CURRENT_TIMESTAMP,
		changed_date TEXT DEFAULT CURRENT_TIMESTAMP

	);
	DELETE FROM dog;
    """

    cur = con.cursor()
    cur.executescript(stmt)
    

def insert_dog(dog):
    cur = con.cursor()
    cur.execute("INSERT INTO dog (uuid, first_name, last_name, age, sex, temperament) VALUES (?,?,?,?,?,?)", 
        (
            dog.id,
            dog.first_name,
            dog.last_name,
            dog.age,
            dog.sex,
            dog.temperament,
        )
    )
    con.commit()

if __name__ == "__main__":
    create_table()
    
    dogs = import_dogs()

    
    start_time = time.time()

    for dog in dogs:
        insert_dog(dog)

    elapsed_time = time.time() - start_time

    print(f"Inserts took {elapsed_time} seconds")
