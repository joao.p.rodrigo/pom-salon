package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
	_ "modernc.org/sqlite"
)

const dsnURI = "./db/go_db.sqlite3"

func main() {
	db, err := sql.Open("sqlite", dsnURI)
	// db, err := sql.Open("sqlite", dsnURI)
	if err != nil {
		panic(err)
	}

	create_schema(db)
	addDogs(db)
}

func create_schema(db *sql.DB) {
	stmt := `
    PRAGMA journal_mode=WAL;
    PRAGMA busy_timeout=5000;
    PRAGMA foreign_keys=1;

	CREATE TABLE IF NOT EXISTS dog (
		id INT PRIMARY KEY,
		uuid TEXT UNIQUE NOT NULL,
		first_name TEXT NOT NULL,
		last_name TEXT NOT NULL,
		age INT NOT NULL,
		sex INT NOT NULL,
		temperament INT NOT NULL,
		created_date TEXT DEFAULT CURRENT_TIMESTAMP,
		changed_date TEXT DEFAULT CURRENT_TIMESTAMP

	);
	DELETE FROM dog;
	`

	_, err := db.Exec(stmt)
	if err != nil {
		panic(err)
	}
}

func addDogs(db *sql.DB) {
	dogData, err := ioutil.ReadFile("assets/poms.csv")
	if err != nil {
		panic(err)
	}

	dogs := []dog{}

	for _, line := range strings.Split(string(dogData), "\n") {
		splitData := strings.Split(line, ",")
		if len(splitData) < 2 {
			continue
		}
		age, _ := strconv.Atoi(splitData[3])
		sex, _ := strconv.Atoi(splitData[4])
		temperament, _ := strconv.Atoi(splitData[5])
		d := dog{
			ID:          uuid.MustParse(splitData[0]),
			FirstName:   splitData[1],
			LastName:    splitData[2],
			Age:         age,
			Sex:         sexType(sex),
			Temperament: temperamentType(temperament),
		}
		dogs = append(dogs, d)
	}

	fmt.Println("begin inserts")
	start := time.Now()
	for _, d := range dogs {
		insertDog(db, d)
	}
	elapsed := time.Since(start)

	fmt.Printf("Inserts took %v seconds\n", elapsed.Seconds())
}

func insertDog(db *sql.DB, d dog) {
	stmt := `INSERT INTO dog (uuid, first_name, last_name, age, sex, temperament) VALUES (?,?,?,?,?,?)`

	_, err := db.Exec(stmt, d.ID, d.FirstName, d.LastName, d.Age, d.Sex, d.Temperament)
	if err != nil {
		panic(err)
	}
}
