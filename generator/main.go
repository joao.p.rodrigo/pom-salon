package main

import (
	"fmt"
	"os"
)

func main() {
	gen := dogGenerator{}
	gen.loadDefaultData()

	maxPoms := 100000

	dogFile, err := os.Create("assets/poms.csv")
	if err != nil {
		panic(err)
	}

	defer dogFile.Close()
	for i := 0; i < maxPoms; i++ {
		pom := *gen.mustRandom()
		fmt.Fprintf(dogFile, "%s,%s,%s,%v,%v,%v\n", pom.ID, pom.FirstName, pom.LastName, pom.Age, pom.Sex, pom.Temperament)
	}
}
