package main

import (
	"fmt"

	"github.com/google/uuid"
)

type temperamentType int

const (
	temperamentCalm = iota
	temperamentUneasy
	temperamentAgressive
)

type sexType int

const (
	sexMale = iota
	sexFemale
)

// dog is the struct containing general information about a dog.
type dog struct {
	ID          uuid.UUID       `json:"id"`
	FirstName   string          `json:"firstName"`
	LastName    string          `json:"lastName"`
	Age         int             `json:"age"`
	Sex         sexType         `json:"sex"`
	Temperament temperamentType `json:"temperament"`
}

func (do *dog) String() string {
	return fmt.Sprintf("pom %s %s, %v years old", do.FirstName, do.LastName, do.Age)
}
