package main

import (
	_ "embed"
	"errors"
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/google/uuid"
)

//go:embed male_names.txt
var maleNamesData string

//go:embed female_names.txt
var femaleNamesData string

//go:embed last_names.txt
var lastNamesData string

func init() {
	rand.Seed(time.Now().Unix())
}

// dogGenerator holds dogs names and enables creation of random dogs with
// the .random() and .mustRandom()
//
// dogGenerator must be initialized with either default data or slices of
// dog names in order to work.
type dogGenerator struct {
	maleNames   []string
	femaleNames []string
	lastNames   []string
}

func (d *dogGenerator) loadDefaultData() {
	d.femaleNames = strings.Split(femaleNamesData, "\n")
	d.maleNames = strings.Split(maleNamesData, "\n")
	d.lastNames = strings.Split(lastNamesData, "\n")
}

// random returns a dog with a random id and random traits.
// If the dogGenerator is not correctly initialized, random will throw
// an error
func (d *dogGenerator) random() (*dog, error) {
	// ensure names are loaded
	if err := d.loadedNames(); err != nil {
		return nil, fmt.Errorf("random: %w", err)
	}

	sex := sexType(rand.Intn(2))

	var firstNamesList []string

	if sex == sexFemale {
		firstNamesList = d.femaleNames
	} else {
		firstNamesList = d.maleNames
	}

	return &dog{
		ID:          uuid.New(),
		Age:         rand.Intn(10) + 1,
		Sex:         sex,
		LastName:    randomChoiceStr(d.lastNames),
		Temperament: temperamentType(rand.Intn(3)),
		FirstName:   randomChoiceStr(firstNamesList),
	}, nil
}

// mustRandom returns a random dog or panics if there is an error.
func (d *dogGenerator) mustRandom() *dog {
	randDog, err := d.random()
	if err != nil {
		panic(err)
	}
	return randDog
}

func (d *dogGenerator) loadedNames() error {
	if len(d.femaleNames) == 0 || len(d.maleNames) == 0 || len(d.lastNames) == 0 {
		return errors.New("loadedNames: not all dog names are loaded")
	}

	return nil
}

func randomChoiceStr(choices []string) string {
	return choices[rand.Intn(len(choices))]
}
